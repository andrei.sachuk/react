import React from "react";
import {Theme, useTheme} from "app/providers/ThemeProvider";
import cls from './ThemeSwitcher.module.scss';
import {classNames} from "shared/lib/ClassNames/classNames";
import LightIcon from "shared/assets/icons/theme-light.svg"
import DarkIcon from "shared/assets/icons/theme-dark.svg"
import {Button} from "shared/ui/Button/ui/Button";

interface ThemeSwitcherProps {
    className?: string;
}

export const ThemeSwitcher = ({className}: ThemeSwitcherProps) => {
    const {theme, toggleTheme} = useTheme()

    return (
        <Button className={classNames(cls.themeSwitcher, {}, [className])}
                onClick={toggleTheme}>{theme === Theme.LIGHT ? <LightIcon/> : <DarkIcon/>}</Button>

    );
};