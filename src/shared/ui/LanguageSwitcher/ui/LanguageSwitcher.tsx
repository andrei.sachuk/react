import cls from './LanguageSwitcher.module.scss';
import {classNames} from 'shared/lib/ClassNames/classNames';
import {useTranslation} from "react-i18next";
import {Button} from "shared/ui/Button";

interface LanguageSwitcherProps {
    className?: string;
}

export const LanguageSwitcher = ({className}: LanguageSwitcherProps) => {
    const {t, i18n} = useTranslation()
    const toggleLanguage = async () => {
        i18n.changeLanguage(i18n.language === 'ru' ? 'en' : 'ru')
    }
    return (
        <Button className={classNames(cls.languageSwitcher, {}, [className])}
                onClick={toggleLanguage}>{t("Язык")}</Button>
    );
};